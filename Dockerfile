FROM wodby/drupal-solr:8-7.2

USER root

COPY new-solr-core.sh /opt/docker-solr/scripts

RUN chown solr:solr /opt/docker-solr/scripts/new-solr-core.sh && \
    chmod +x /opt/docker-solr/scripts/new-solr-core.sh

USER $SOLR_USER